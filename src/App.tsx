import React from 'react';
import { Provider } from 'mobx-react';

import { WeatherInfo } from './components/WeatherDetails/WeatherDetails';
import { stores } from './stores/stores';

import './App.scss';

function App() {
  return (
    <div className="App">
      <Provider {...stores}>
        <WeatherInfo />
      </Provider>
    </div>
  );
}

export default App;
