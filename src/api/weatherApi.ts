import { axiosInstance } from './axiosInstance';
import { IWeatherCondition } from '../models/weatherCondition';

export interface IWeatherApi {
  fetchCurrentWeather: (latitude: number, longitude: number) => Promise<IWeatherCondition>;
}

export class WeatherApi implements IWeatherApi {

  public fetchCurrentWeather = async (latitude: number, longitude: number) => {
    try {
      const response = await axiosInstance.get('/current', { params: { query: `${latitude},${longitude}` } });
      return response.data.current;
    } catch(e) {
      // TODO: handle error
    }
  }
}
