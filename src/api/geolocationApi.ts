export interface IPosition {
  readonly coords: Coordinates;
  readonly timestamp: number;
}

export interface IGeolocationApi {
  getGeolocation: () => Promise<IPosition>;
}

export class GeolocationApi {
  private getLocationTimeout = 10 * 1000;
  private geoLocationOptions = { timeout: this.getLocationTimeout };

  public getGeolocation = (): Promise<IPosition> => {
    return new Promise((resolve, reject) => {
      window.navigator.geolocation.getCurrentPosition(
        position => resolve(position),
        positionError => reject(positionError),
        this.geoLocationOptions
      );
    });
  };
}
