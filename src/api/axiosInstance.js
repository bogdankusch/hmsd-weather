import axios from 'axios';

import { axiosConfig } from './axiosConfig';

export const axiosInstance = axios.create(axiosConfig);
