import React, { Component } from 'react';
import { observer, inject } from 'mobx-react';
import InputRange from 'react-input-range';

import { WEATHER_STORE, IWeatherStore } from '../../stores/weatherStore';
import { GEOLOCATION_STORE, IGeoLocationStore } from '../../stores/geoLocationStore';

import 'react-input-range/lib/css/index.css';
import './style.scss';

interface IProps {
  [WEATHER_STORE]?: IWeatherStore;
  [GEOLOCATION_STORE]?: IGeoLocationStore;
}

@inject(GEOLOCATION_STORE, WEATHER_STORE)
@observer
export class WeatherInfo extends Component<IProps> {
  private temperatureHue = {
    min: 180,   // #00ffff
    middle: 58, // #fff700
    max: 33,    // #ff8c00
  };
  private inputMaxRange = 30;
  private inputMiddleRange = 10;
  private inputMinRange = -10;

  get weatherStore(){
    return this.props[WEATHER_STORE]!;
  }

  get geolocationStore(){
    return this.props[GEOLOCATION_STORE]!;
  }

  async componentDidMount(){
    await this.geolocationStore.initCurrentLocation();
    await this.weatherStore.fetchWeatherCondition(this.geolocationStore.latitude, this.geolocationStore.longitude);
  }

  calculateHue = (temperature: number) => {
    const minHue = this.temperatureHue.min;
    const middleHue = this.temperatureHue.middle;
    const maxHue = this.temperatureHue.max;

    if(temperature <= this.inputMinRange) {
      return minHue;
    } else if(temperature >= this.inputMaxRange) {
      return maxHue;
    } else if(temperature > this.inputMinRange && temperature <= this.inputMiddleRange) {
      const tempKoef = (temperature - this.inputMinRange)/(this.inputMiddleRange - this.inputMinRange);
      return minHue + (middleHue - minHue) * tempKoef;
    } else {
      const tempKoef = (temperature - this.inputMiddleRange)/(this.inputMaxRange - this.inputMiddleRange);
      return middleHue + (maxHue - middleHue) * tempKoef;
    }
  }

  calculateBackgroundColor = (temperature: number) => {
    const saturationLightness = '100%, 50%';
    const huePrecision = 2;
    return `hsl(${this.calculateHue(+temperature).toFixed(huePrecision)}, ${saturationLightness})`;
  }


  render() {
    const backgroundStyle = {backgroundColor: this.calculateBackgroundColor(this.weatherStore.temperature)};

    return (
      <div>
        <div className={'weather-icon_container'} style={backgroundStyle} >
          <img className={'weather-icon'} src={this.weatherStore.iconPath} alt={'weather icon'} />
        </div>
        <div className={'slider_container'}>
          <InputRange
            maxValue={this.inputMaxRange}
            minValue={this.inputMinRange}
            value={this.weatherStore.temperature}
            //@ts-ignore
            onChange={this.weatherStore.setTemperature}
          />
        </div>
      </div>
    )
  }
}
