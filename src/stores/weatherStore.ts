import { observable, action, computed } from 'mobx';

import { IWeatherApi } from '../api/weatherApi';

export const WEATHER_STORE = 'WEATHER_STORE';

export interface IWeatherStore {
  temperature: number;

  iconPath: string;

  setTemperature: (temperature: number) => void;
  fetchWeatherCondition: (latitude: number, longitude: number) => Promise<void>;
}

export class WeatherStore implements IWeatherStore {
  private weatherApi: IWeatherApi;

  @observable temperature: number = 0;
  @observable weatherIcons: Array<string> = [];

  @computed
  get iconPath(){
    return this.weatherIcons.length > 0 ? this.weatherIcons[0] : ''
  }

  constructor(weatherApi: IWeatherApi) {
    this.weatherApi = weatherApi;
  }

  @action.bound
  setTemperature = (temperature: number) => {
    this.temperature = temperature;
  }

  @action.bound
  public fetchWeatherCondition = async (latitude: number, longitude: number) => {
    try {
      const weather = await this.weatherApi.fetchCurrentWeather(latitude, longitude);
      this.temperature = weather.temperature;
      this.weatherIcons = weather.weather_icons;
    } catch(e) {
      //TODO: handle error
    }
  }
}
