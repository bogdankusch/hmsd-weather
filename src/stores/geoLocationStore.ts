import { observable, action } from 'mobx';

import { IGeolocationApi } from '../api/geolocationApi';

export const GEOLOCATION_STORE = 'GEO_LOCATION_STORE';

export interface IGeoLocationStore {
  latitude: number;
  longitude: number;

  initCurrentLocation: () => Promise<void>;
}

export class GeoLocationStore implements IGeoLocationStore {
  private geolocationApi: IGeolocationApi;

  @observable public latitude = 0;
  @observable public longitude = 0;

  public get isGeolocationAllowd() {
    return !!window.navigator.geolocation;
  }

  constructor(geolocationApi: IGeolocationApi) {
    this.geolocationApi = geolocationApi;
  }

  @action.bound
  public initCurrentLocation = async () => {
    try {
      const position = await this.geolocationApi.getGeolocation();

      this.latitude = position.coords.latitude;
      this.longitude = position.coords.longitude;
    } catch (error) {
      // TODO: throw error
    }
  };
}
