import { WeatherStore, WEATHER_STORE } from './weatherStore';
import { WeatherApi } from '../api/weatherApi';
import { GEOLOCATION_STORE, GeoLocationStore } from './geoLocationStore';
import { GeolocationApi } from '../api/geolocationApi';

const weatherApi = new WeatherApi();
const geolocationApi = new GeolocationApi();

const weatherStore = new WeatherStore(weatherApi);
const geoLocationStore = new GeoLocationStore(geolocationApi);

export const stores = {
  [WEATHER_STORE]: weatherStore,
  [GEOLOCATION_STORE]: geoLocationStore
}
